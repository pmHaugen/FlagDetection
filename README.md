# Flag Detection

Using Nvidia JETANK [Jetank AI KIT](https://www.waveshare.com/jetank-ai-kit.htm).

## Contents
In this project we programmed the robot to track two different flags. If it see a ukrain flag it will follow it, but if it sees a russain flag it will look away.
![](FlagTracking.mp4)


## Contribute
* Thomas Haaland

* Paal Marius Bruun Haugen 
